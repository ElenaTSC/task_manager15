package ru.tsk.ilina.tm.service;

import ru.tsk.ilina.tm.api.repository.IProjectRepository;
import ru.tsk.ilina.tm.api.service.IProjectService;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyIndexException;
import ru.tsk.ilina.tm.exception.empty.EmptyNameException;
import ru.tsk.ilina.tm.exception.empty.EmptyStatusException;
import ru.tsk.ilina.tm.exception.entity.ProjectNotFoundException;
import ru.tsk.ilina.tm.exception.system.IndexIncorrectException;
import ru.tsk.ilina.tm.model.Project;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project removeByID(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeByID(id);
    }

    @Override
    public Project removeByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project removeByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeByName(name);
    }

    @Override
    public Project findByID(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findByID(id);
    }

    @Override
    public Project findByIndex(final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project findByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findByName(name);
    }

    @Override
    public void create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.add(project);
    }

    @Override
    public void remove(Project project) {
        if (project == null) throw new ProjectNotFoundException();
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        if (comparator == null) return Collections.emptyList();
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = projectRepository.findByID(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        final Project project = projectRepository.findByIndex(index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startByID(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.startByID(id);
    }

    @Override
    public Project startByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        return projectRepository.startByIndex(index);
    }

    @Override
    public Project startByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.startByName(name);
    }

    @Override
    public Project finishByID(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.finishByID(id);
    }

    @Override
    public Project finishByIndex(Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        return projectRepository.finishByIndex(index);
    }

    @Override
    public Project finishByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.finishByName(name);
    }

    @Override
    public Project changeStatusByID(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByID(id, status);
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByIndex(index, status);
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        return projectRepository.changeStatusByName(name, status);
    }

}
