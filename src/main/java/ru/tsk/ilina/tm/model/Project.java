package ru.tsk.ilina.tm.model;

import ru.tsk.ilina.tm.api.entity.IWBS;
import ru.tsk.ilina.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Project implements IWBS {

    private String id = UUID.randomUUID().toString();
    private String name = "";
    private String description = "";
    private Status status = Status.NOT_STARTED;
    private Date startDate;
    private Date finishDate;
    private Date createdDate = new Date();

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Project() {

    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    @Override
    public void setCreatedDate(Date createdDate) {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + " : " + name;
    }

}
