package ru.tsk.ilina.tm.api.controller;

public interface IProjectTaskController {

    void findTaskByProjectId();

    void bindTaskToProjectById();

    void unbindTaskToProjectById();

    void removeAllTaskByProjectId();

}
