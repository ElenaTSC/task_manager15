package ru.tsk.ilina.tm.api.controller;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void removeByID();

    void removeByIndex();

    void removeByName();

    void findByID();

    void findByIndex();

    void findByName();

    void updateById();

    void updateByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

}
